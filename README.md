Moved
=====

This crate is published separately but is no longer maintained in its own Git
repository. It is now part of
[oc-wasm-rust](https://gitlab.com/Hawk777/oc-wasm-rust).

About
=====

OC-Wasm-Applied-Energistics provides high-level APIs for accessing components
provided by [Applied
Energistics](https://appliedenergistics.github.io/ae2-site-archive/) for
applications running in the [OC-Wasm](https://gitlab.com/Hawk777/oc-wasm)
architecture.
